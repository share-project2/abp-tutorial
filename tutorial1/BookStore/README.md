# Abp tutorial 1
Permission project 

# Start project:
## 1. Install Abp Cli v7.4.2
```dotnet tool install --global Volo.Abp.Cli --version 7.4.2```


## 2. Install ef tool:
```dotnet tool install --global dotnet-ef```

## 3.Create Abp project:
```abp new BookStore -t app-nolayers```
```bash
ABP CLI 7.4.2
Creating your project...
Project name: BookStore
Template: app-nolayers
Output folder: /Users/duyhung/git/dotnet/abp-tutorial/tutorial1
Using cached template: app-nolayers, version: 7.4.2
Theme: LeptonXLite
...
```

## 4.Update ConnectionString in "appsettings.json"
```json
"ConnectionStrings": {
    "Default": "Data Source=Your-IP-Address,1433;Database=Your-Database;User Id=sa;Password=Your-Password;TrustServerCertificate=True;"
  },
```
## 5. Init DB 
Run command in your project (in the same folder of the .csproj file)
```bash
cd BookStore
dotnet run --migrate-database 
```
```bash
...
Successfully completed all database migrations
```

## 6. Run project:
Right click project:
=> Build project
![Build project](images/1.build_project.png)

=> Run Project
![Run project](images/2.run_project.png)
## 7. Browse:
https://localhost:44378/

![Open project](images/3.homepage.png)
